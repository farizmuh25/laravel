FROM php:8.0-apache

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql

# RUN docker-php-ext-install mbstring zip exif
RUN docker-php-ext-install zip exif
RUN docker-php-ext-install pcntl
RUN docker-php-ext-configure gd
RUN docker-php-ext-install gd


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www/html

RUN composer install

RUN cp .env.example .env


RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/storage/* /var/www/html/bootstrap/cache /var/www/html/public

COPY default.conf /etc/apache2/sites-enabled/000-default.conf

EXPOSE 80

RUN a2enmod rewrite headers

COPY start.sh /usr/local/bin/start

RUN chmod u+x /usr/local/bin/start

CMD ["/usr/local/bin/start"]

